import com.ncorti.ktfmt.gradle.tasks.KtfmtFormatTask

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.devtools.ksp")
    id("com.ncorti.ktfmt.gradle") version "0.17.0"
}

android {
    namespace = "com.example.forgottenumbrella.cardboardmuseum"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.example.forgottenumbrella.cardboardmuseum"
        minSdk = 19
        targetSdk = 34
        versionCode = 18
        versionName = "2.2.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJunitRunner"

        // Needed because apk is too big and targets min SDK < 20.
        multiDexEnabled = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    kotlin {
        jvmToolchain(17)
    }

    // Needed for Robolectric.
    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}

dependencies {
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.fragment:fragment-ktx:1.6.2")
    // Needed due to apk size.
    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.preference:preference-ktx:1.2.1")
    implementation("androidx.work:work-runtime-ktx:2.9.0")
    implementation("com.google.android.apps.muzei:muzei-api:3.4.1")
    implementation("com.squareup.moshi:moshi:1.15.0")
    implementation(platform("com.squareup.okhttp3:okhttp-bom:4.11.0"))
    implementation("com.squareup.okhttp3:okhttp")
    implementation("com.squareup.okhttp3:logging-interceptor")
    implementation("com.squareup.retrofit2:converter-moshi:2.9.0")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.google.android.material:material:1.11.0")
    ksp("com.squareup.moshi:moshi-kotlin-codegen:1.15.0")
}

ktfmt {
    kotlinLangStyle()
}

// https://github.com/cortinico/ktfmt-gradle?tab=readme-ov-file#using-with-a-pre-commit-hook-
// NOTE: doesn't support configuration cache
tasks.register<KtfmtFormatTask>("ktfmtPrecommit") {
    description = "Format files specified by --include-only option (defaults to all files)"
    source = project.fileTree(projectDir)
    include("src/**/*.kt")
}
