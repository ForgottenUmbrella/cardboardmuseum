package com.example.forgottenumbrella.cardboardmuseum

import android.net.Uri
import android.util.Log
import com.google.android.apps.muzei.api.provider.Artwork
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.IOException
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

/** Service that fetches artworks from Gelbooru. */
internal class GelbooruService : ArtService {
    companion object {
        /** Combine settings into a full list of tags. */
        private fun fullTags(settings: Settings, deviceInfo: DeviceInfo): String {
            return listOfNotNull(
                    settings.tags,
                    // Muzei does not support videos (and GIFs are low quality).
                    "-animated",
                    if (settings.safeMode) "rating:general" else null,
                    if (settings.random) "sort:random" else null,
                    if (settings.ratio) {
                        // on API Version < 30, this won't include the status
                        // bar height, however a range of 10% in both parameters
                        // should include the exact resolution of the device
                        val (height, width) =
                            if (!deviceInfo.isRotated) Pair(deviceInfo.height, deviceInfo.width)
                            else Pair(deviceInfo.width, deviceInfo.height)
                        val (minWidth, maxWidth) = width * 90 / 100 to width * 110 / 100
                        val (minHeight, maxHeight) = height * 90 / 100 to height * 110 / 100
                        "width:>=${minWidth} width:<=${maxWidth} height:>=${minHeight} height:<=${maxHeight}"
                    } else null
                )
                .joinToString(" ")
        }
    }

    /** The underlying Gelbooru API service. */
    private val api = GelbooruRetrofitService.create()

    @Throws(IOException::class, ApiException::class)
    override fun getArtworks(settings: Settings, deviceInfo: DeviceInfo): List<Artwork> {
        val response =
            api.listPosts(
                    tags = fullTags(settings, deviceInfo),
                    userId = settings.username,
                    apiKey = settings.key
                )
                .execute()
        val body = response.body() ?: throw ApiException(response.errorBody()?.string())
        val posts = body.post ?: listOf()
        Log.d(LOG_TAG, "First post: ${posts.getOrNull(0)}")
        return posts
            .filter { post -> post.id != null && post.fileUrl != null }
            .map { post ->
                val tagLimit = 10
                val title =
                    post.tagList.take(tagLimit).joinToString(" ") +
                        if (post.tagList.count() > tagLimit) " ..." else ""
                Artwork.Builder()
                    .token(post.id)
                    .title(title)
                    .attribution(post.source)
                    .persistentUri(Uri.parse(post.fileUrl))
                    .webUri(Uri.parse(post.webUrl))
                    .build()
            }
    }

    override fun previewUri(settings: Settings, deviceInfo: DeviceInfo): Uri {
        val authParams =
            if (settings.username != null && settings.key != null) {
                "&user_id=${settings.username}&api_key=${settings.key}"
            } else {
                ""
            }
        return Uri.parse(
            "https://gelbooru.com/index.php?page=post&s=list&tags=${fullTags(settings, deviceInfo)}${authParams}"
        )
    }
}

/** [Retrofit] wrapper over the Gelbooru API. */
internal interface GelbooruRetrofitService {
    companion object {
        /** Return an instance of the service. */
        fun create(): GelbooruRetrofitService {
            val loggingInterceptor =
                HttpLoggingInterceptor { Log.d(LOG_TAG, it) }
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            val loggingClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
            return Retrofit.Builder()
                .baseUrl("https://gelbooru.com")
                .client(loggingClient)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create()
        }
    }

    /**
     * Return an object containing a list of image posts.
     *
     * @param tags Filter the set of images to those matching the given space-delimited tags.
     * @param userId A user ID for API requests to avoid restrictions.
     * @param apiKey A key for API requests to avoid restrictions.
     */
    @GET("/index.php?page=dapi&s=post&q=index&json=1")
    fun listPosts(
        @Query("tags") tags: String? = null,
        @Query("user_id") userId: String? = null,
        @Query("api_key") apiKey: String? = null,
    ): Call<Posts>

    @JsonClass(generateAdapter = true)
    data class Posts(
        /** An array of posts. Null if there are no matching posts. */
        val post: List<Post>?,
    )

    @JsonClass(generateAdapter = true)
    data class Post(
        /** A unique post ID. Null if the artist is banned. */
        val id: String?,
        /** The source of the image. May be empty. */
        val source: String,
        /** Tags that have been applied to the image. Space-delimited list. */
        val tags: String,
        /** URL to the image file. Null if the post is inaccessible (censored or banned). */
        @Json(name = "file_url") val fileUrl: String?,
    ) {
        /** A list of tags. */
        val tagList: List<String>
            get() = tags.split(" ")

        /** The human-friendly URL to the image. */
        val webUrl: String
            get() = "https://gelbooru.com/index.php?page=post&s=view&id=${id}"
    }
}
