package com.example.forgottenumbrella.cardboardmuseum

import android.Manifest
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.android.apps.muzei.api.provider.MuzeiArtProvider
import com.google.android.apps.muzei.api.provider.ProviderClient
import com.google.android.apps.muzei.api.provider.ProviderContract
import java.io.IOException

/** Application-wide tag for logging. */
internal const val LOG_TAG = "CardboardMuseum"

/**
 * [MuzeiArtProvider] implementation for Cardboard Museum.
 *
 * Fetches art from Gelbooru (temporary) via a background [ArtProviderWorker].
 */
class ArtProvider : MuzeiArtProvider() {
    override fun onLoadRequested(initial: Boolean) {
        val context =
            context
                ?: throw IllegalStateException("Missing context to enqueue work request to add art")
        ArtProviderWorker.enqueueLoad(context)
    }
}

/** [Worker] that adds artworks to Muzei. */
internal class ArtProviderWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {
    companion object {
        /** Input data key for whether to refresh the list of artworks. */
        private const val DATA_REFRESH = "refresh"

        /** ID for the error notification channel. */
        private const val CHANNEL_ERROR_ID = "errors"

        /** ID for the error notification. */
        private const val NOTIFICATION_ERROR_ID = 0

        /** ID for the [PendingIntent] request to open the app. */
        private const val INTENT_LAUNCH_ID = 0

        /**
         * Start the worker.
         *
         * @param refresh Whether to replace the list of artworks with a new set.
         */
        fun enqueueLoad(context: Context, refresh: Boolean = false) {
            val networkConstraints =
                Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build()
            val inputData = Data.Builder().putBoolean(DATA_REFRESH, refresh).build()
            val workRequest =
                OneTimeWorkRequestBuilder<ArtProviderWorker>()
                    .setConstraints(networkConstraints)
                    .setInputData(inputData)
                    .build()
            WorkManager.getInstance(context).enqueue(workRequest)
        }

        /**
         * Add artworks to Muzei.
         *
         * Internal with dependency injection for ease of testing.
         */
        internal fun doWork(
            context: Context,
            provider: ProviderClient =
                ProviderContract.getProviderClient(context, context.packageName),
            settings: Settings = Settings.load(context),
            artService: ArtService = ArtService.create(settings),
            refresh: Boolean = false,
        ): Result {
            val artworks =
                try {
                    artService.getArtworks(settings, DeviceInfo(context))
                } catch (e: IOException) {
                    Log.w(LOG_TAG, "Error reading response", e)
                    return Result.retry()
                } catch (e: ApiException) {
                    Log.e(LOG_TAG, "Api error", e)
                    notifyError(context, e.getLocalisedMessage(context))
                    return Result.failure()
                }
            Log.d(LOG_TAG, "First artwork: ${artworks.getOrNull(0)}")

            if (artworks.isEmpty()) {
                Log.e(LOG_TAG, "No images returned from API")
                notifyError(context, context.getString(R.string.error_no_results_text))
                return Result.failure()
            }

            if (refresh) {
                provider.setArtwork(artworks.first())
                provider.addArtwork(artworks.drop(1))
            } else {
                provider.addArtwork(artworks)
            }
            return Result.success()
        }

        /**
         * Send a notification alerting the user of a failure to change the wallpaper.
         *
         * Internal for testing.
         */
        internal fun notifyError(context: Context, reason: String) {
            val canNotify =
                Build.VERSION.SDK_INT < 33 ||
                    ActivityCompat.checkSelfPermission(
                        context,
                        Manifest.permission.POST_NOTIFICATIONS
                    ) == PackageManager.PERMISSION_GRANTED
            if (!canNotify) {
                return
            }

            val notificationManager = NotificationManagerCompat.from(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel =
                    NotificationChannel(
                        CHANNEL_ERROR_ID,
                        context.getString(R.string.notification_channel_errors_name),
                        NotificationManager.IMPORTANCE_LOW
                    )
                channel.description =
                    context.getString(R.string.notification_channel_errors_description)
                channel.setShowBadge(true)
                notificationManager.createNotificationChannel(channel)
            }
            val notification = createErrorNotification(context, reason)
            notificationManager.notify(NOTIFICATION_ERROR_ID, notification)
        }

        /** Return a notification object for errors. */
        @SuppressLint("InlinedApi") // Upstream example suggests this is safe to ignore.
        private fun createErrorNotification(context: Context, reason: String): Notification {
            val intent = Intent(context, SettingsActivity::class.java)
            val launchIntent =
                PendingIntent.getActivity(
                    context,
                    INTENT_LAUNCH_ID,
                    intent,
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
                )
            return NotificationCompat.Builder(context, CHANNEL_ERROR_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle(context.getString(R.string.notification_error_title))
                .setContentText(reason)
                .setStyle(NotificationCompat.BigTextStyle().bigText(reason))
                .setContentIntent(launchIntent)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .build()
        }
    }

    override fun doWork() =
        doWork(applicationContext, refresh = inputData.getBoolean(DATA_REFRESH, false))
}
