package com.example.forgottenumbrella.cardboardmuseum

import android.net.Uri
import android.util.Log
import androidx.core.net.toUri
import com.google.android.apps.muzei.api.provider.Artwork
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import java.io.IOException
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

/** A service that fetches artworks from Danbooru. */
internal class DanbooruService(
    private val retrofit: DanbooruRetrofitService = DanbooruRetrofitService.create(),
) : ArtService {
    companion object {
        /** Combine settings into a full list of tags. */
        private fun fullTags(settings: Settings, deviceInfo: DeviceInfo): String {
            var tags = settings.tags ?: ""
            if (settings.safeMode) {
                tags += " rating:general"
            }
            if (settings.ratio) {
                if (deviceInfo.height == 0 || deviceInfo.width == 0) {
                    // Avoid division by zero (should never happen).
                    return tags
                }
                // on API Version < 30, this won't include the status
                // bar height, however a range of 10% in the ratio parameter
                // should include the actual aspect ratio of the device
                var ratio = deviceInfo.width.toFloat() / deviceInfo.height.toFloat()
                if (deviceInfo.isRotated) {
                    // Flip the ratio to match the device's default orientation.
                    ratio = 1 / ratio
                }
                val minRatio = String.format("%.2f", ratio * 0.9)
                val maxRatio = String.format("%.2f", ratio * 1.1)
                tags += " ratio:>=${minRatio} ratio:<=${maxRatio}"
            }
            return tags
        }
    }

    @Throws(IOException::class)
    override fun getArtworks(settings: Settings, deviceInfo: DeviceInfo): List<Artwork> {
        // We try to get as many images as possible in one query to avoid duplicates when
        // settings.random is enabled.
        // However, the API may time out if it takes too long to find enough matching images.
        // We thus halve the number of requested images each time this happens.
        val limits = arrayOf(200, 100, 50, 25, 12, 5)
        for ((i, limit) in limits.withIndex()) {
            val response =
                retrofit
                    .listPosts(
                        tags = fullTags(settings, deviceInfo),
                        random = settings.random,
                        username = settings.username,
                        key = settings.key,
                        limit = limit,
                    )
                    .execute()
            val artworks =
                response
                    .body()
                    ?.filterNot(DanbooruRetrofitService.Post::isAnimated)
                    ?.mapNotNull(DanbooruRetrofitService.Post::toArtwork)

            if (artworks == null) {
                val errorJson = response.errorBody()?.string()
                Log.d(LOG_TAG, "Raw error: $errorJson")
                val error = errorJson?.let(Error::fromJson)
                if ((error?.error == Error.QUERY_CANCELLED_ERROR)) {
                    if (i != limits.size - 1) {
                        Log.d(
                            LOG_TAG,
                            "Query took too long for limit=${limit}, reducing limit and re-trying"
                        )
                        continue
                    } else {
                        Log.d(
                            LOG_TAG,
                            "Query took too long for limit=${limit}, giving up on retrying"
                        )
                    }
                }
                throw ApiException(error?.message)
            }
            return artworks
        }
        return emptyList()
    }

    override fun previewUri(settings: Settings, deviceInfo: DeviceInfo): Uri {
        val authParams =
            if (settings.username != null && settings.key != null) {
                "&login=${settings.username}&api_key=${settings.key}"
            } else {
                ""
            }
        return Uri.parse(
            "https://danbooru.donmai.us/posts?tags=${fullTags(settings, deviceInfo)}&random=${settings.random}${authParams}"
        )
    }

    /**
     * A JSON object representing an API error on Danbooru.
     *
     * Automatic adapter generation by [JsonClass] annotation requires that it not be private.
     */
    @JsonClass(generateAdapter = true)
    data class Error(val error: String, val message: String, val backtrace: List<String>) {
        companion object {
            /** The [error] value for errors caused by the API cancelling the query. */
            const val QUERY_CANCELLED_ERROR = "ActiveRecord::QueryCanceled"
            /** Instantiate and return an [Error] from a [json] string. */
            fun fromJson(json: String) =
                Moshi.Builder().build().adapter(Error::class.java).fromJson(json)
        }
    }
}

/** [Retrofit] wrapper over the Danbooru API. */
internal interface DanbooruRetrofitService {
    companion object {
        /** Return an implementation of [DanbooruRetrofitService]. */
        fun create(): DanbooruRetrofitService {
            val interceptor =
                HttpLoggingInterceptor { Log.d(LOG_TAG, it) }
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
            return Retrofit.Builder()
                .baseUrl("https://danbooru.donmai.us")
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create()
        }
    }

    /** Return a callable for a list of posts matching the given [tags]. */
    @GET("/posts.json")
    fun listPosts(
        @Query("tags") tags: String? = null,
        @Query("random") random: Boolean? = null,
        @Query("login") username: String? = null,
        @Query("api_key") key: String? = null,
        @Query("limit") limit: Int? = null,
    ): Call<List<Post>>

    /** A JSON object representing a post on Danbooru. */
    @JsonClass(generateAdapter = true)
    data class Post(
        /** Used for token and webUri. Null if artist is banned. */
        val id: String?,
        /** Used for title. */
        @Json(name = "tag_string_character") val characters: String,
        /** Used for title. */
        @Json(name = "tag_string_copyright") val copyrights: String,
        /** Used for byline. */
        @Json(name = "tag_string_artist") val artists: String,
        /** Used for attribution. */
        val source: String,
        /** Used for persistentUri. Null if post is censored or banned. */
        @Json(name = "file_url") val fileUrl: String?,
        /** Used for metadata. */
        @Json(name = "tag_string") val tags: String,
        /** Used for metadata. */
        val rating: String,
    ) {
        companion object {
            /**
             * Return a grammatical list of at most [limit] [tags] with underscores replaced with
             * spaces.
             */
            private fun normaliseTags(tags: String, limit: Int? = null): String {
                val tagList = tags.split(" ")
                val limitedTags =
                    if (limit != null && tagList.size > limit) {
                        tagList.take(limit) + "${tagList.size - limit} more"
                    } else {
                        tagList
                    }
                return grammaticalListOf(limitedTags.map { it.replace("_", " ") })
            }

            /**
             * Return a string of [items] separated by commas and ultimately the conjunction "and".
             */
            private fun grammaticalListOf(items: List<String>) =
                if (items.size > 1) {
                    // Oxford comma only applies for lists of size three or greater.
                    val conjunction = if (items.size >= 3) ", and " else " and "
                    "${items.dropLast(1).joinToString(postfix = conjunction)}${items.last()}"
                } else {
                    items.firstOrNull() ?: ""
                }
        }

        /** Convert to a Muzei [Artwork], or return null if the post is missing required fields. */
        fun toArtwork(): Artwork? {
            return Artwork.Builder()
                .token(id ?: return null)
                .title(caption())
                .byline(normaliseTags(artists))
                .attribution(source)
                .persistentUri(fileUrl?.toUri() ?: return null)
                .webUri("https://danbooru.donmai.us/posts/$id".toUri())
                .metadata(Metadata(tags, rating).toJson())
                .build()
        }

        /** Return whether the post is tagged as animated. */
        fun isAnimated() = tags.split(" ").contains("animated")

        /** Return the caption as seen on Danbooru, except the artist attribution. */
        private fun caption(): String {
            val characters = normaliseTags(characters, limit = 5)
            val copyrights = normaliseTags(copyrights, limit = 1)
            return if (characters.isNotEmpty()) "$characters ($copyrights)" else copyrights
        }

        /**
         * Artwork metadata of interest (mainly for testing) that can be (de)serialised to/from
         * JSON.
         */
        @JsonClass(generateAdapter = true)
        data class Metadata(val tags: String, val rating: String) {
            companion object {
                /** Instantiate and return [Metadata] from a [json] string. */
                fun fromJson(json: String) =
                    Moshi.Builder().build().adapter(Metadata::class.java).fromJson(json)
            }

            /** Convert and return a JSON string from [Metadata]. */
            // Instance cannot be null, so return value cannot be null either.
            fun toJson() = Moshi.Builder().build().adapter(Metadata::class.java).toJson(this)!!
        }
    }
}
