package com.example.forgottenumbrella.cardboardmuseum

import android.content.Context
import android.hardware.display.DisplayManager
import android.os.Build
import android.util.DisplayMetrics
import android.view.Display
import android.view.Surface
import android.view.WindowManager

/** Information about the device dimensions with respect to its current orientation. */
class DeviceInfo(context: Context) {
    /**
     * Display width in pixels. This will be greater than the height when the device is in landscape
     * orientation.
     */
    public val width: Int
    /**
     * Display height in pixels. This will be less than the width when the device is in landscape
     * orientation.
     */
    public val height: Int
    /**
     * Whether the device has been rotated away from its default orientation (ignoring symmetry).
     * For the purpose of aspect ratio calculations, a rotation of 180 degrees is ignored.
     */
    public val isRotated: Boolean

    init {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val displayManager = context.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        val defaultDisplay = displayManager.getDisplay(Display.DEFAULT_DISPLAY)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val metrics = windowManager.maximumWindowMetrics
            height = metrics.bounds.height()
            width = metrics.bounds.width()
        } else {
            val metrics = DisplayMetrics()
            // getMetrics was deprecated in Android 30, however android
            // studio will still show a warning below due to:
            // https://issuetracker.google.com/issues/228611948
            defaultDisplay.getMetrics(metrics)
            // This height/width doesn't contain the status bar height
            height = metrics.heightPixels
            width = metrics.widthPixels
        }
        val rotation = defaultDisplay.rotation
        isRotated = rotation == Surface.ROTATION_270 || rotation == Surface.ROTATION_90
    }
}
