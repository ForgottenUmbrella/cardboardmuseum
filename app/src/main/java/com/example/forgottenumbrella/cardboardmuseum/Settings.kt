package com.example.forgottenumbrella.cardboardmuseum

import android.content.Context
import androidx.preference.PreferenceManager

/** Settings exposed by [SettingsActivity]. */
internal data class Settings(
    val imageboard: String,
    val tags: String?,
    val safeMode: Boolean,
    val random: Boolean,
    val ratio: Boolean,
    val username: String?,
    val key: String?,
) {
    companion object {
        /** Instantiate and return settings from shared preferences. */
        fun load(context: Context): Settings {
            val prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return Settings(
                imageboard =
                    prefs.getString(context.getString(R.string.pref_imageboard), null)
                        ?: context.getString(R.string.pref_imageboard_default),
                tags =
                    prefs
                        .getString(
                            context.getString(R.string.pref_tags),
                            context.getString(R.string.pref_tags_default)
                        )
                        ?.ifBlank { null },
                safeMode =
                    prefs.getBoolean(
                        context.getString(R.string.pref_safe_mode),
                        context.resources.getBoolean(R.bool.pref_safe_mode_default),
                    ),
                random =
                    prefs.getBoolean(
                        context.getString(R.string.pref_random),
                        context.resources.getBoolean(R.bool.pref_random_default)
                    ),
                ratio =
                    prefs.getBoolean(
                        context.getString(R.string.pref_ratio),
                        context.resources.getBoolean(R.bool.pref_ratio_default)
                    ),
                username =
                    prefs
                        .getString(
                            context.getString(R.string.pref_username),
                            context.getString(R.string.pref_username_default)
                        )
                        ?.ifBlank { null },
                key =
                    prefs
                        .getString(
                            context.getString(R.string.pref_key),
                            context.getString(R.string.pref_key_default)
                        )
                        ?.ifBlank { null },
            )
        }
    }

    /** Constructor with default values. */
    constructor(
        context: Context,
        imageboard: String = context.getString(R.string.pref_imageboard_default),
        tags: String? = context.getString(R.string.pref_tags_default).ifBlank { null },
        safeMode: Boolean = context.resources.getBoolean(R.bool.pref_safe_mode_default),
        random: Boolean = context.resources.getBoolean(R.bool.pref_random_default),
        ratio: Boolean = context.resources.getBoolean(R.bool.pref_ratio_default),
        username: String? = context.getString(R.string.pref_username_default).ifBlank { null },
        key: String? = context.getString(R.string.pref_key_default).ifBlank { null },
    ) : this(imageboard, tags, safeMode, random, ratio, username, key)
}
