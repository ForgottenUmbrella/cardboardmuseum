package com.example.forgottenumbrella.cardboardmuseum

import android.content.Context
import android.util.Log

/** Exception type for API errors from the imageboard. */
class ApiException(message: String?) : Exception(message) {
    /** Return the localised reason for the error. */
    fun getLocalisedMessage(context: Context) =
        when (val message = localizedMessage) {
            "Missing `login` or `api_key`" -> context.getString(R.string.error_missing_auth_text)
            "Invalid API key" -> context.getString(R.string.error_invalid_auth_text)
            "You cannot search for more than 2 tags at a time." ->
                context.getString(R.string.error_tag_limit_text)
            "The database timed out running your query." ->
                context.getString(R.string.error_timeout)
            "" -> {
                Log.wtf(LOG_TAG, "Empty error message")
                message
            }
            null -> {
                Log.wtf(LOG_TAG, "Null error message")
                ""
            }
            else -> {
                Log.wtf(LOG_TAG, "Missing localisation: $message")
                message
            }
        }
}
