# Steps to create a release
1. Bump version name and version code in `app/build.gradle.kts`
2. Update `CHANGELOG.md`
3. Build release APK
4. Create tag: `git tag -a vX.Y.Z`
5. Push to upstream: `git push --follow-tags`
6. Create release
