# 2.2.0

- Add option to only use images that match the device aspect ratio/dimensions.
  Thanks @sin3point14!

# 2.1.0

- Reinstate support for Danbooru.
- Allow selecting between Gelbooru and Danbooru as wallpaper sources.

# 2.0.0

- Source wallpapers from Gelbooru instead of Danbooru, which has restricted programmatic access.

# 1.2.3

- Be more strict about what counts as a "safe for work" image, in line with Danbooru's rating system
  changes. Thanks @Battezu!
- Update libraries for Android 12 support.

# 1.2.2

- Support dark mode.
- Fix image preview.
- Fix "Couldn't view artwork details" error.
- Fix accidentally setting wallpaper to every new image when settings have changed.
- Fix setting wallpaper to an incompatible video.
- Fix caption overflow when there are too many characters or copyrights.

# 1.2.1

- Fix build.

# 1.2.0

- Make randomness an option in response to Danbooru API change. Note that this counts towards your
  tag limit.
- Request even more images at once.

# 1.1.9

- Request more images at once to increase randomness.
- Fix failure to add/refresh artworks when attempting to retrieve banned or censored posts.
- Support package visibility in Android 11.
- Explain why "Preview images" button may fail.

# 1.1.8

- Update libraries for Android 10 support.

# 1.1.7

- Fix artwork cache not clearing when settings are changed.
- Fix grammatical conflict in byline with Muzei notification.

# 1.1.6

- Fix app closing on launch.

# 1.1.5

- Fix build failure.

# 1.1.4

- (nothing changed; this tag is only necessary so screenshots can be added)

# 1.1.3

- Fix failure to instantiate worker.
- Fix safe mode.
- Fix JSON handling.

# 1.1.2

- Show API key description instead of actual key in summary.
- Show "Not set" instead of a blank summary when the preference is unset.

# 1.1.1

- Properly handle authentication and account limit errors.

# 1.1.0

- When you change the settings, new wallpapers will now immediately reflect those changes.
- Authentication errors and errors with too many tags are now handled.

# 1.0.0

- Supports the Muzei 3.0 API.
- Changes wallpaper using Danbooru as a provider.
- Images are filtered by given tags.
- Safe mode to (try to) avoid NSFW wallpapers.
- Preview button allows a preview of images matching the given tags so you know what you're in for.
